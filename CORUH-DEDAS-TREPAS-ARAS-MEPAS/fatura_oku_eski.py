#!/usr/bin/env python3

from datetime import datetime
from pathlib import Path
import pdfplumber
import traceback
import openpyxl
import math
import re

##################################################
# Bu Program Yahya YILDIRIM tarafından kodlanmıştır.
# kod.pardus.org.tr/yahyayildirim
# yahya.yildirim@diyanet.gov.tr
##################################################
# Kurulması gereken kütüphaneler
##################################################
# Uçbirimden Kontrol Edin : python3 --version
# Python 3.x sürümünün kurulu olması gerekiyor..
##################################################
# Aşağıdaki kodları sırasıyla uçbirimde çalıştırın.
# sudo apt install python3-pip
# sudo python3 -m pip install --upgrade pip
# sudo python3 -m pip install --upgrade setuptools
# sudo python3 -m pip install --upgrade pdfplumber
# sudo python3 -m pip install --upgrade openpyxl
# sudo python3 -m pip install --upgrade typing_extentions
#################################################

#Tarih
bugun = datetime.today()
tarih = datetime.strftime(bugun, '%d%m%Y_%H%M%S')

# Taslak Excel Dosyamız
excel_dosyasi = openpyxl.load_workbook('ornek_dosya.xlsx')

# Excel dosyamızda bulunan sayfa adımız
excel_sayfasi = excel_dosyasi['Faturalar']

# PDF dosyasından çektiğimiz tarihlerin formatını değiştirme
def tarih_donustur(tarih):
	return re.sub(r'(\d{4}).?(\d{1,2}).?(\d{1,2})', '\\3.\\2.\\1', tarih)

def fatura_oku():
	try:
		# PDF dosyalaromızın .pdf uzantılı olması gerekiyor.
		# Bu programın bulunduğu klasör içinde PDF isimli bir klasör açarak içine tüm fatura pdflerini kopyalayın.
		pdf_klasoru = Path('./PDF/').rglob('*.pdf')

		# PDF Klasörü içindeki her bir dosyayı buluyoruz.
		files = [x for x in pdf_klasoru]

		# PDF dosyası kadar sıra numarası vermek için 0 dan başlatıyoruz.
		sira_no = 0

		# Uçbirimde çıkacak sutun isimleri
		print("{:<5}{:<12}{:<18}{:<60}{:<10}{:<12}{:<10}".format("S.N", "ABONE NO", "FATURA NO", "ABONE/CAMİİ ADI", "DÖNEMİ", "SON ÖDEME", "ÖD.TUTAR"))
		
		# for döngüsü ile her bir PDF dosyasını okuyup gerekli olan bilgileri çekiyoruz.
		for file in files:
			sayfalar = []

			# pdfplumber modülü ile dosyaları açıyoruz
			with pdfplumber.open(file) as pdf:
				ilksayfa = pdf.pages[0]
				sayfalar.append(ilksayfa.extract_text())
			#print(sayfalar)

			devirGecikme = float(0)
			# Sayfaları Geziyoruz
			for sayfa in sayfalar:
				unvan_re = "SAYIN.*\s(.*).*\s(.*)[CAMİİ?|MH.|Oluşma]"
				unvan_eslesen = re.search(unvan_re, sayfa)

				unvan_temizle = ' '.join(unvan_eslesen.groups()).split("Fatura")[0].split("Son")[0].split("Oluşma Zamanı")[0].split("Kapı No:")[0].split("Kapı")[0].split("No:")[0].replace("MH.", "MAH.")
				if "CAMİ" in unvan_temizle or "CAMI" in unvan_temizle:
					abone_adi = f"{unvan_temizle.split('CAMİ')[0].split('CAMI')[0].strip()} CAMİİ"
				else:
					abone_adi = unvan_temizle
				#print(abone_adi)

				kwh_liste = []
				kwh_toplam = 0
				kwh_eslesme = re.findall(r"ToplamTuketim.([0-9]+.?[0-9]+.?\d+)|(\d+.\d+.\d+).?KWH|(\d+.\d+).?KWH|(\d+).?KWH", sayfa)
				for kwh in kwh_eslesme:
					kwh_liste.append(float(' '.join(kwh).strip().split()[0].replace('.', '').replace(',', '.')))
					kwh_toplam = round((sum(kwh_liste)), 3)

				tesisat_re = "TESISATNO.?\s(\d+)|Tüket.c. No.?\s(\d+)"
				tesisat_eslesen = re.findall(tesisat_re, sayfa)
				for tesisat in tesisat_eslesen:
					tuketici_no = ' '.join(tesisat).strip()

				fatura_re = "Fatura\sNo[:]?\s\w+"
				fatura_eslesen = re.findall(fatura_re, sayfa)
				for fatura in fatura_eslesen:
					fatura_no = fatura.split()[-1].strip()

				tuketici_grubu_eslesen = re.search("Tek(.*)\n", sayfa)
				for grupbul in tuketici_grubu_eslesen.groups():
					if "Mesken" in grupbul:
						grubu = "Mesken"
					elif "Aydınlatma" in grupbul:
						grubu = "Aydınlatma"
					elif "Ticarethane" in grupbul:
						grubu = "Ticarethane"
					else:
						grubu = "Bulunamadı"

				sonOkuma_re = "Son\sOkuma:\s\w+.\w+.\w+|Son\sOkuma\s\w+\s\w+.\w+.\w+|ILKOKUMA.?\w+.\w+.\w+"
				sonOkuma_eslesen = re.findall(sonOkuma_re, sayfa)
				for okuma in sonOkuma_eslesen:
					sonOkuma_tarihi = tarih_donustur(okuma.split()[-1].split("#")[-1].strip())
					faturaDonemi_yil = sonOkuma_tarihi.split(".")[-1]
					faturaDonemi_ay = sonOkuma_tarihi.split(".")[-2]
					faturaDonemi = faturaDonemi_yil + "/" + faturaDonemi_ay

				ilkOkuma_re = ".lk\sOkuma:\s\w+.\w+.\w+|.lk\sOkuma\s\w+\s\w+.\w+.\w+|SONOKUMA.?\w+.\w+.\w+"
				ilkOkuma_eslesen = re.findall(ilkOkuma_re, sayfa)
				for okuma in ilkOkuma_eslesen:
					ilkOkuma_tarihi = tarih_donustur(okuma.split()[-1].split("#")[-1].strip())

				sonOdeme_re = "Son\s.deme.*|SON.DEME.*"
				sonOdeme_eslesen = re.findall(sonOdeme_re, sayfa)
				for okuma in sonOdeme_eslesen:
					sonOdeme_tarihi = tarih_donustur(okuma.split(":")[-1].split()[-1].split("#")[-1].strip())

				# vergiler_re = "[V]erg.ler.*\s[0-9]+.?[0-9]+.?\d+\sTL"
				# vergiler_eslesen = re.findall(vergiler_re, sayfa)
				# for vergi in vergiler_eslesen:
				# 	vgtt = float(vergi.split()[-2].strip().replace('.', '').replace(',', '.').strip())
				# 	damga_vergisi = round(((vgtt/0.08)*0.00948), 2)

				kdvmatrah_re = "KDV\D+\(\S+\)\W+(.?.?.?.?.?.?.?.?.?.?)\W+TL"
				kdvmatrah_eslesen = re.findall(kdvmatrah_re, sayfa)
				kdvmatrah = float(''.join(kdvmatrah_eslesen).replace('.', '').replace(',', '.').strip())

				damga_vergisi = (kdvmatrah * 0.00948)

				kdv_re = "KDV\(\S+\)\W+(.?.?.?.?.?.?.?.?.?)\W+TL"
				kdv_eslesen = re.findall(kdv_re, sayfa)
				kdv = float(''.join(kdv_eslesen).replace('.', '').replace(',', '.').strip())

				odenecekTutar_re = ".denecek\s.*\s[0-9]+.?[0-9]+.?\d+\sTL"
				odenecekTutar_eslesen = re.findall(odenecekTutar_re, sayfa)
				for tutar in odenecekTutar_eslesen:
					odenecekTutar = float(tutar.split()[-2].strip().replace('.', '').replace(',', '.'))

				devirGecikme_re = "Devir Gecikme\s.*\sTL"
				devirGecikme_eslesen = re.findall(devirGecikme_re, sayfa)
				for devir in devirGecikme_eslesen:
					devirGecikme = float(devir.split()[-2].strip().replace('.', '').replace(',', '.'))

			# # Her döngüde sira numaramızı bir arttırıyoruz
			sira_no = sira_no + 1

			# # Yaptığımız işlemin uçbirimden çıktısını görmek için print ile ekrana yazdırıyoruz.
			print("{:<5}{:<12}{:<18}{:<60}{:<10}{:<12}{:<10}".format(sira_no, tuketici_no, fatura_no, abone_adi, faturaDonemi, sonOdeme_tarihi, odenecekTutar))

			# # ornek_excel.xlsx dosyasının son satırını buluyoruz.
			son_satir = excel_sayfasi.max_row

			# # ornek_excel.xlsx dosyasının sırasıyla değerlerimizi yazdırıyoruz.
			excel_sayfasi.cell(column=1, row=son_satir + 1).value = sira_no
			excel_sayfasi.cell(column=2, row=son_satir + 1).value = tuketici_no
			excel_sayfasi.cell(column=3, row=son_satir + 1).value = fatura_no
			excel_sayfasi.cell(column=4, row=son_satir + 1).value = abone_adi
			excel_sayfasi.cell(column=5, row=son_satir + 1).value = grubu
			excel_sayfasi.cell(column=6, row=son_satir + 1).value = faturaDonemi
			excel_sayfasi.cell(column=7, row=son_satir + 1).value = ilkOkuma_tarihi
			excel_sayfasi.cell(column=8, row=son_satir + 1).value = sonOkuma_tarihi
			excel_sayfasi.cell(column=9, row=son_satir + 1).value = sonOdeme_tarihi
			excel_sayfasi.cell(column=10, row=son_satir + 1).value = kwh_toplam
			excel_sayfasi.cell(column=11, row=son_satir + 1).value = kdvmatrah
			excel_sayfasi.cell(column=12, row=son_satir + 1).value = damga_vergisi
			excel_sayfasi.cell(column=13, row=son_satir + 1).value = kdv
			excel_sayfasi.cell(column=14, row=son_satir + 1).value = odenecekTutar
			excel_sayfasi.cell(column=15, row=son_satir + 1).value = devirGecikme

			# # ornek_excel.xlsx dosyasını pdf_fatura_excel_aktarma.xlsx olarak kaydediyoruz.
			# # pdf_fatura_excel_aktarma.xlsx olarak yeni bir excel dosyası hazırlamış oluyoruz.
			excel_dosyasi.save('pdf_fatura_excel_aktarma_' + tarih + '.xlsx')

	except:
		print("Dosya Adı: ", file)
		print(traceback.format_exc())

if __name__ == "__main__":
	fatura_oku()
