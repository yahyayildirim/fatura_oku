#!/usr/bin/env python3

from datetime import datetime
from pathlib import Path
import natsort
import traceback
import openpyxl
import math
import fitz
import re

##################################################
# Bu Program Yahya YILDIRIM tarafından kodlanmıştır.
# kod.pardus.org.tr/yahyayildirim
# yahya.yildirim@diyanet.gov.tr
##################################################
# Kurulması gereken kütüphaneler
##################################################
# Uçbirimden Kontrol Edin : python3 --version
# Python 3.x sürümünün kurulu olması gerekiyor..
##################################################
# Aşağıdaki kodları sırasıyla uçbirimde çalıştırın.
# sudo apt install python3-pip
# sudo python3 -m pip install --upgrade pip
# sudo python3 -m pip install --upgrade pdmupdf
# sudo python3 -m pip install --upgrade natsort
# sudo python3 -m pip install --upgrade openpyxl
#################################################

#Tarih
bugun = datetime.today()
tarih = datetime.strftime(bugun, '%d%m%Y_%H%M%S')

# Taslak Excel Dosyamız
excel_dosyasi = openpyxl.load_workbook('ornek_dosya.xlsx')

# Excel dosyamızda bulunan sayfa adımız
excel_sayfasi = excel_dosyasi['Faturalar']

# PDF dosyasından çektiğimiz tarihlerin formatını değiştirme
def tarih_donustur(tarih):
	return re.sub(r'(\d{4}).?(\d{1,2}).?(\d{1,2})', '\\3.\\2.\\1', tarih)

def fatura_oku():
	try:
		# PDF dosyalaromızın .pdf uzantılı olması gerekiyor.
		# Bu programın bulunduğu klasör içinde PDF isimli bir klasör açarak içine tüm fatura pdflerini kopyalayın.
		pdf_klasoru = Path('./PDF/').rglob('*.pdf')

		# PDF Klasörü içindeki her bir dosyayı buluyoruz.
		files = [x for x in pdf_klasoru]

		# PDF dosyası kadar sıra numarası vermek için 0 dan başlatıyoruz.
		sira_no = 0

		# Uçbirimde çıkacak sutun isimleri
		print("{:<5}{:<50}{:<10}".format("S.N", "ABONE/CAMİİ ADI", "ÖD.TUTAR"))

		# for döngüsü ile her bir PDF dosyasını okuyup gerekli olan bilgileri çekiyoruz.
		for file in natsort.os_sorted(files):
			birincisayfa = []

			# pdfplumber modülü ile dosyaları açıyoruz
			with fitz.open(file) as pdf:
				sayfa_1 = pdf[0]
				text = sayfa_1.get_text()
				birincisayfa.append(text.replace('\n', ' ').upper())
			#print(birincisayfa)

			# Sayfaları Geziyoruz
			for sayfa in birincisayfa:
				# Cami/Abone adını tespit etmek için kriter belirliyoruz.
				unvan_eslesen = re.search("SAYIN\s([0-9]*?\D+)", sayfa)
				unvan_temizle = ''.join(unvan_eslesen.groups()).split("FATURA")[0].split("SON")[0].split("OLUŞMA ZAMANI")[0].split("KAPI NO:")[0].split("KAPI")[0].split("NO:")[0].replace("MH.", "MAH.").strip()
				if "CAMİ" in unvan_temizle or "CAMI" in unvan_temizle:
					abone_adi = f"{unvan_temizle.split('CAMİ')[0].split('CAMI')[0].strip()} CAMİİ"
				else:
					abone_adi = unvan_temizle

				kwh_liste = []
				kwh_toplam = 0
				kwh_eslesme = re.findall(r"TOPLAMT.KET.M.([0-9]+.?[0-9]+.?\d+)|(\d+.\d+.\d+).?KWH|(\d+.\d+).?KWH|(\d+).?KWH", sayfa)
				for kwh in kwh_eslesme:
					kwh_liste.append(float(' '.join(kwh).strip().split()[0].replace('.', '').replace(',', '.')))
					kwh_toplam = round((sum(kwh_liste)), 3)

				tesisat_eslesen = re.findall(r"TESISATNO.?\s(\d+)|T.KET.C. NO.?\s(\d+)", sayfa)
				for tesisat in tesisat_eslesen:
					tuketici_no = ' '.join(tesisat).strip()

				fatura_re = "FATURA\sNO[:]?\s\w+"
				fatura_eslesen = re.findall(fatura_re, sayfa)
				for fatura in fatura_eslesen:
					fatura_no = fatura.split()[-1].strip()

				tuketici_grubu_eslesen = re.search("GRUBU(.*)", sayfa)
				for grupbul in tuketici_grubu_eslesen.groups():
					if "MESKEN" in grupbul:
						grubu = "MESKEN"
					elif "AYDINLATMA" in grupbul:
						grubu = "AYDINLATMA"
					elif "TICARETHANE" in grupbul:
						grubu = "TİCARETHANE"
					elif "SERBEST" in grupbul:
						grubu = "SERBEST"
					else:
						grubu = "DİĞER"

				sonOkuma_eslesen = re.search('SON\sOKUMA:\s(\w+.\w+.\w+)|SON\sOKUMA\s\w+\s(\w+.\w+.\w+)|[Iİ]LKOKUMA.?(\w+.\w+.\w+)', sayfa)
				sonOkuma_tarihi = tarih_donustur(''.join(sonOkuma_eslesen.group().split('#')[-1].split()[-1]))
				faturaDonemi = sonOkuma_tarihi.split(".")[-1] + "/" + sonOkuma_tarihi.split(".")[-2]

				ilkOkuma_eslesen = re.search('[Iİ]LK\sOKUMA:\s(\w+.\w+.\w+)|[Iİ]LK\sOKUMA\s\w+\s(\w+.\w+.\w+)|SONOKUMA.?(\w+.\w+.\w+)', sayfa)
				ilkOkuma_tarihi = tarih_donustur(''.join(ilkOkuma_eslesen.group().split('#')[-1].split()[-1]))

				sonOdeme_eslesen = re.search('SON\s?.DEME\D+(\S+)', sayfa)
				sonOdeme_tarihi = tarih_donustur(''.join(sonOdeme_eslesen.groups()))

				vergiler_eslesen = re.search('[V]ERG\D+DAH\D+(\S*)', sayfa)
				vgtt = float(''.join(vergiler_eslesen.group().split()[-1].replace('.', '').replace(',', '.')))

				kdv_eslesen = re.search('(HESAPL\D+K.?D.?V.?|KDV)\s?\(.\S*\)\D+(\S*)', sayfa)
				kdv = float(''.join(kdv_eslesen.group().split()[-1].replace('.', '').replace(',', '.')))

				odenecekTutar_eslesen = re.search('DENECEK\sTUTAR\D+(\S+)\sTL', sayfa)
				odenecek_tutar = float(''.join(odenecekTutar_eslesen.groups()).replace('.', '').replace(',', '.'))

			# Her döngüde sira numaramızı bir arttırıyoruz
			sira_no = sira_no + 1

			# Yaptığımız işlemin uçbirimden çıktısını görmek için print ile ekrana yazdırıyoruz.
			print("{:<5}{:<50}{:<10}".format(sira_no, abone_adi, odenecek_tutar))

			# ornek_excel.xlsx dosyasının son satırını buluyoruz.
			son_satir = excel_sayfasi.max_row

			# ornek_excel.xlsx dosyasının sırasıyla değerlerimizi yazdırıyoruz.
			excel_sayfasi.cell(column=1, row=son_satir + 1).value = sira_no
			excel_sayfasi.cell(column=2, row=son_satir + 1).value = tuketici_no
			excel_sayfasi.cell(column=3, row=son_satir + 1).value = fatura_no
			excel_sayfasi.cell(column=4, row=son_satir + 1).value = abone_adi
			excel_sayfasi.cell(column=5, row=son_satir + 1).value = grubu
			excel_sayfasi.cell(column=6, row=son_satir + 1).value = faturaDonemi
			excel_sayfasi.cell(column=7, row=son_satir + 1).value = ilkOkuma_tarihi
			excel_sayfasi.cell(column=8, row=son_satir + 1).value = sonOkuma_tarihi
			excel_sayfasi.cell(column=9, row=son_satir + 1).value = sonOdeme_tarihi
			excel_sayfasi.cell(column=10, row=son_satir + 1).value = kwh_toplam
			excel_sayfasi.cell(column=11, row=son_satir + 1).value = vgtt
			excel_sayfasi.cell(column=12, row=son_satir + 1).value = kdv
			excel_sayfasi.cell(column=13, row=son_satir + 1).value = odenecek_tutar

			# ornek_excel.xlsx dosyasını pdf_fatura_excel_aktarma.xlsx olarak kaydediyoruz
			# pdf_fatura_excel_aktarma.xlsx olarak yeni bir excel dosyası hazırlamış oluyoruz.
			excel_dosyasi.save('pdf_fatura_excel_aktarma_' + tarih + '.xlsx')

	except:
		print("Dosya Adı: ", file)
		print(traceback.format_exc())

if __name__ == "__main__":
	fatura_oku()
