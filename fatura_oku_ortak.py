#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pathlib import Path
import natsort
import pathlib 
import shutil
import fitz
import re
import os

# PDF dosyalaromızın .pdf uzantılı olması gerekiyor.
# Bu programın bulunduğu klasör içinde PDF isimli bir klasör açarak içine tüm fatura pdflerini kopyalayın.
pdf_klasoru = Path('./PDF/').rglob('*.pdf')
files = [x for x in pdf_klasoru]

def tarih_donustur(tarih):
    return re.sub(r'(\d{4}).?(\d{1,2}).?(\d{1,2})', '\\3.\\2.\\1', tarih)

def klasor_olustur(sirket_adi):
    try:
        os.makedirs(sirket_adi)
    except OSError:
        pass

# PDF dosyası kadar sıra numarası vermek için 0 dan başlatıyoruz.
sira_no = 0

# Uçbirimde çıkacak sutun isimleri
print("{:<5}{:<35}{:<20}{:<16}{:<10}".format("S.N", "ELEKTRİK DAĞ. ŞİRKETİ", "FATURA NO", "SON ÖDEME TAR.", "ÖDENECEK TUTAR"))

# for döngüsü ile her bir PDF dosyasını okuyup gerekli olan bilgileri çekiyoruz.
for file in natsort.os_sorted(files):
    birincisayfa = []
    # pdfplumber modülü ile dosyaları açıyoruz
    with fitz.open(file) as pdf:
        sayfa_1 = pdf[0]
        text = sayfa_1.get_text()
        birincisayfa.append(text.strip().replace('\n', ' ').upper())
    print(file)
    #print(birincisayfa)

    sirketler = ['AKEDAŞ', 'AKSA FIRAT', 'DİCLE', 'YEPAŞ', 'ÇAMLIBEL', 'AKSA ÇORUH', 'TRAKYA', 'AYDEM', 'SAKARYA', 'OSMANGAZİ ELEKTRİK', 'ARAS', 'ENERJİSA BAŞKENT', 'ENERJİSA İSTANBUL', 'YEŞİLIRMAK', 'MERAM', 'KAYSERİ', 'LİMAK ULUDAĞ', 'GEDİZ', 'AKDENİZ']

    sira_no += 1
    # Sayfaları Geziyoruz
    for sayfa in birincisayfa:
        for sirket in sirketler:
            if sirket in ''.join(sayfa.splitlines()):
                firma_adi = '{}'.format(sirket.upper())

        faturano_eslesen = re.search('(FATURA (\D+)?NO)\D+(\S{16})', sayfa)
        fatura_no = ''.join(faturano_eslesen.group()).split()[-1].strip()

        tesisatno_eslesen = re.search('(TES[Iİ]SAT|T[ÜU]KET[İI]C[Iİ]|REFERANS)\D+(\d+)', sayfa)
        tesisat_no = ''.join(tesisatno_eslesen.group()).split()[-1].split('#')[-1]

        odenecek_eslesen = re.search('(FATURA\D+TUTARI|ÖDENECEK\D+TUTAR|TL):?\s([0-9]+.?[0-9]+.?\d+)', sayfa)
        odenecek_tutar = odenecek_eslesen.group(2)

        sonOdeme_eslesen = re.search('(SON\D+DE\D+|Z.)(\d{2}[\./-]\d{2}[\./-]\d{4}|\d{4}[\./-]\d{2}[\./-]\d{2})', sayfa)
        sonOdeme_Tarihi = tarih_donustur(sonOdeme_eslesen.group(2)).replace('/', '.').replace('-', '.')

        #print("#######################################################################################################")
        print("{:<5}{:<35}{:<20}{:<16}{:<10}".format(sira_no, firma_adi, fatura_no, sonOdeme_Tarihi, odenecek_tutar))

        #print(sira_no, firma_adi, fatura_no, sonOdeme_Tarihi, odenecek_tutar)



# DİYANET ÖDENEK TALEBİ İÇİN İSTENENLER
# İBADETHANE ABONELİK ADI
# SÖZLEŞME/ ABONE/ MÜŞTERİ/ TESİSAT/ HESAP NO*
# CAMİ GRUBU (A, B, C, D)
# SON ÖDEME TARİHİ
# FATURA TUTARI (TL)


# Fatura Tarihi 
# Son Ödeme Tarihi +
# Fatura Kodu +
# Sözleşme Hesap No
# Tesisat No +
# Faturanın Ait Olduğu Yer
# Fatura Tutarı 
# Muhtelif Mahsup 
# Yuvarlama Farkı 
# Mahsuptan Sonrası Fatura Tutarı 
# %8 KDV Matrahı 
# %18 KDV Matrahı 
# %8 KDV Tutarı 
# %18 KDV Tutarı 
# Damga Vergisi 
# Ödenecek Net Tutar 