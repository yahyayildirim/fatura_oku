#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
from pathlib import Path
import natsort
import traceback
import openpyxl
import math
import fitz
import re

##################################################
# Bu Program Yahya YILDIRIM tarafından kodlanmıştır.
# kod.pardus.org.tr/yahyayildirim
# yahya.yildirim@diyanet.gov.tr
##################################################
# Kurulması gereken kütüphaneler
##################################################
# Uçbirimden Kontrol Edin : python3 --version
# Python 3.x sürümünün kurulu olması gerekiyor..
##################################################
# Aşağıdaki kodları sırasıyla uçbirimde çalıştırın.
# sudo apt install python3-pip
# sudo python3 -m pip install --upgrade pip
# sudo python3 -m pip install --upgrade pdmupdf
# sudo python3 -m pip install --upgrade natsort
# sudo python3 -m pip install --upgrade openpyxl
#################################################

#Tarih
bugun = datetime.today()
tarih = datetime.strftime(bugun, '%d%m%Y_%H%M%S')

# Taslak Excel Dosyamız
excel_dosyasi = openpyxl.load_workbook('ornek_dosya.xlsx')

# Excel dosyamızda bulunan sayfa adımız
excel_sayfasi = excel_dosyasi['Faturalar']

def fatura_oku():
    try:
        # PDF dosyalaromızın .pdf uzantılı olması gerekiyor.
        # Bu programın bulunduğu klasör içinde PDF isimli bir klasör açarak içine tüm fatura pdflerini kopyalayın.
        pdf_klasoru = Path('./PDF/').rglob('*.pdf')

        # PDF Klasörü içindeki her bir dosyayı buluyoruz.
        files = [x for x in pdf_klasoru]

        # PDF dosyası kadar sıra numarası vermek için 0 dan başlatıyoruz.
        sira_no = 0

        # Uçbirimde çıkacak sutun isimleri
        print("{:<5}{:<18}{:<40}{:<12}{:<12}".format("S.N", "ABONE NO", "ABONE ADI", "FAT.TUTARI", "ÖDE.TUTAR"))

        # for döngüsü ile her bir PDF dosyasını okuyup gerekli olan bilgileri çekiyoruz.
        for file in natsort.os_sorted(files):
            birincisayfa = []
            # pdfplumber modülü ile dosyaları açıyoruz
            with fitz.open(file) as pdf:
                sayfa_1 = pdf[0]
                text = sayfa_1.get_text()
                birincisayfa.append(text.replace('\n', ' ').upper())
            #print(birincisayfa)

            sira_no = sira_no + 1

            # Sayfaları Geziyoruz
            for sayfa in birincisayfa:
                # Cami/Abone adını tespit etmek için kriter belirliyoruz.
                abone_eslesen = re.search("(SAYIN)(.*)(CAMİİ|CAMİ|CAM|AYDIN)", sayfa)
                abone_adres = ''.join(abone_eslesen.group(2)).strip().split("ÖDENECEK")[-1].split("NO:")[0].strip()

                if "CAMİ" in abone_adres or "CAMI" in abone_adres:
                    abone_adi = f"{abone_adres.split('CAMİ')[0].split('CAMI')[0].strip()} CAMİİ"
                    if abone_adi.startswith("TUTAR"):
                        abone_adi = ' '.join(abone_adi.split()[2::])
                else:
                    abone_adi = abone_adres
                    if abone_adi.startswith("TUTAR"):
                        abone_adi = ' '.join(abone_adi.split()[2::])

                # Toplam harcanan KWH
                kwhler = []
                kwh_eslesen = re.findall("K[0-9]\D+\s(\d+\S+)", sayfa)
                for kw in kwh_eslesen:
                    kwhler.append(round(float(kw.replace(".", "").replace(',', '.')), 3))
                kwh = sum(kwhler)

                # Fatura Tesisat/Tüketici Numarası
                tesisat_eslesen = re.search("TES[Iİ]SAT\D+(\d+)", sayfa)
                tesisat_no = ''.join(tesisat_eslesen.groups()).strip()

                # Müsteri NO
                musteri_eslesen = re.search("M..TER.\sNO.?(\d+)", sayfa)
                musteri_no = ' '.join(musteri_eslesen.groups())

                # Fatura Tesisat/Tüketici Numarası
                sozhesap_eslesen = re.search("S.Z\D+HESA[PB].\s?\D+(\d+)", sayfa)
                sozhesap_no = ' '.join(sozhesap_eslesen.groups()).strip()

                # Fatura No
                faturano_eslesen = re.search("FATURA\D+NO:?\s(\D+\S+)", sayfa)
                fatura_no = ''.join(faturano_eslesen.groups()).strip()

                # Fatura Grubu
                faturagrubu_eslesen = re.search("GRUBU\D+\s(\D+)\s.G", sayfa)
                fatura_grubu = ' '.join(faturagrubu_eslesen.groups()).strip()

                # Son ödeme Tarihi
                sonOdeme_eslesen = re.search("(SON\D+HI:|Z)\s(\d+.\d+.\d+)", sayfa)
                sonOdeme_tarihi = ''.join(sonOdeme_eslesen.group(2)).strip()

                # İlk Okuma Tarihi
                ilkOkuma_eslesen = re.search("(MA\sG\D+|.LK\sOKUMA\D+)(\w+.\w+.\w+)", sayfa)
                ilkOkuma_tarihi = ''.join(ilkOkuma_eslesen.group(2))

                # Son Okuma Tarihi
                sonOkuma_eslesen = re.search("(MA\sG\D+\S+\D+|SON\sOKUMA\D+)(\w+.\w+.\w+)", sayfa)
                sonOkuma_tarihi = ''.join(sonOkuma_eslesen.group(2))

                # Faturanın ait olduğu dönem
                faturaDonemi_ay = sonOkuma_tarihi.split(".")[1]
                faturaDonemi_yil = sonOkuma_tarihi.split(".")[-1]
                fatura_Donemi = faturaDonemi_ay + "/" + faturaDonemi_yil

                # Faturada bulunan damga versigi
                damgavergisi_eslesen = re.search("DAMGA\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)
                if damgavergisi_eslesen is None:
                    damga_vergisi = 0
                else:
                    damga_vergisi = round(float(damgavergisi_eslesen.group(1).strip()), 2)

                #Faturada bulunan kdv tutarı
                kdvler = []
                kdv_eslesen = re.findall("\sKDV\s?\S+\s?(\d+\S+)", sayfa)
                for k in kdv_eslesen:
                    kdvler.append(round(float(k.strip().replace(".", "").replace(',', '.')), 2))
                kdv = sum(kdvler)

                # # Gecikme Bedeli varsa
                # gecikmebedeli_eslesen = re.search("GEC.KME\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)
                # if gecikmebedeli_eslesen is None:
                #   gecikmebedeli = float(0)
                # else:
                #   gecikmebedeli = float(gecikmebedeli_eslesen.group(1).strip().replace(".", "").replace(',', '.'))

                # # Kesme/Açma Bedeli varsa
                # kesacbedeli_eslesen = re.search("KESME\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)
                # if kesacbedeli_eslesen is None:
                #   kesacbedeli = float(0)
                # else:
                #   kesacbedeli = float(kesacbedeli_eslesen.group(1).strip().replace(".", "").replace(',', '.'))

                #FaturaTutari_eslesen = re.search("[V]ERG\D+DAH\D+(\S*)", sayfa)

                odenecek_tutar_eslesen = re.search("ÖDEN\D+TUT\D+(\S+)", sayfa)
                if odenecek_tutar_eslesen is not None:
                    odenecek_tutar = float(''.join(odenecek_tutar_eslesen.group().split()[-1].replace('.', '').replace(',', '.')))
                else:
                    odenecek_tutar = 0.0

                fatura_tutari_eslesen = re.search("[M]AL\D+[H]\D+[T]OPL\D+[T]\D+(\d+\S+)", sayfa)
                if fatura_tutari_eslesen is not None:
                    fatura_tutari = float(''.join(fatura_tutari_eslesen.group().split()[-1].replace('.', '').replace(',', '.')))
                else:
                    fatura_tutari = odenecek_tutar

            # Yaptığımız işlemin uçbirimden çıktısını görmek için print ile ekrana yazdırıyoruz.
            print("{:<5}{:<18}{:<40}{:<12}{:<12}".format(sira_no, sozhesap_no, abone_adi, kdv, odenecek_tutar))

            # ornek_excel.xlsx dosyasının son satırını buluyoruz.
            son_satir = excel_sayfasi.max_row

            # ornek_excel.xlsx dosyasının sırasıyla değerlerimizi yazdırıyoruz.
            excel_sayfasi.cell(column=1, row=son_satir + 1).value = int(sira_no)
            excel_sayfasi.cell(column=2, row=son_satir + 1).value = int(sozhesap_no)
            excel_sayfasi.cell(column=3, row=son_satir + 1).value = int(tesisat_no)
            excel_sayfasi.cell(column=4, row=son_satir + 1).value = fatura_no
            excel_sayfasi.cell(column=5, row=son_satir + 1).value = abone_adi
            excel_sayfasi.cell(column=6, row=son_satir + 1).value = fatura_grubu
            excel_sayfasi.cell(column=7, row=son_satir + 1).value = fatura_Donemi
            excel_sayfasi.cell(column=8, row=son_satir + 1).value = ilkOkuma_tarihi
            excel_sayfasi.cell(column=9, row=son_satir + 1).value = sonOkuma_tarihi
            excel_sayfasi.cell(column=10, row=son_satir + 1).value = sonOdeme_tarihi
            excel_sayfasi.cell(column=11, row=son_satir + 1).value = kwh
            excel_sayfasi.cell(column=12, row=son_satir + 1).value = fatura_tutari
            excel_sayfasi.cell(column=13, row=son_satir + 1).value = damga_vergisi
            excel_sayfasi.cell(column=14, row=son_satir + 1).value = kdv
            excel_sayfasi.cell(column=15, row=son_satir + 1).value = odenecek_tutar

            # ornek_excel.xlsx dosyasını pdf_fatura_excel_aktarma.xlsx olarak kaydediyoruz.
            # pdf_fatura_excel_aktarma.xlsx olarak yeni bir excel dosyası hazırlamış oluyoruz.
            excel_dosyasi.save('pdf_fatura_excel_aktarma_' + tarih + '.xlsx')
    except:
        print("Dosya Adı: ", file)
        print(traceback.format_exc())

if __name__ == "__main__":
    fatura_oku()
