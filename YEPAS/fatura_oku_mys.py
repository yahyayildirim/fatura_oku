#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
from pathlib import Path
import natsort
import traceback
import openpyxl
import math
import fitz
import re

##################################################
# Bu Program Yahya YILDIRIM tarafından kodlanmıştır.
# kod.pardus.org.tr/yahyayildirim
# yahya.yildirim@diyanet.gov.tr
##################################################
# Kurulması gereken kütüphaneler
##################################################
# Uçbirimden Kontrol Edin : python3 --version
# Python 3.x sürümünün kurulu olması gerekiyor..
##################################################
# Aşağıdaki kodları sırasıyla uçbirimde çalıştırın.
# sudo apt install python3-pip
# sudo python3 -m pip install --upgrade pip
# sudo python3 -m pip install --upgrade pdmupdf
# sudo python3 -m pip install --upgrade natsort
# sudo python3 -m pip install --upgrade openpyxl
#################################################

#Tarih
bugun = datetime.today()
tarih = datetime.strftime(bugun, '%d%m%Y_%H%M%S')

# Taslak Excel Dosyamız
excel_dosyasi = openpyxl.load_workbook('ornek_dosya.xlsx')

# Excel dosyamızda bulunan sayfa adımız
excel_sayfasi = excel_dosyasi['Faturalar']

def fatura_oku():
    try:
        # PDF dosyalaromızın .pdf uzantılı olması gerekiyor.
        # Bu programın bulunduğu klasör içinde PDF isimli bir klasör açarak içine tüm fatura pdflerini kopyalayın.
        pdf_klasoru = Path('./PDF/').rglob('*.pdf')

        # PDF Klasörü içindeki her bir dosyayı buluyoruz.
        files = [x for x in pdf_klasoru]

        # PDF dosyası kadar sıra numarası vermek için 0 dan başlatıyoruz.
        sira_no = 0

        # Uçbirimde çıkacak sutun isimleri
        print("{:<5}{:<18}{:<40}{:<12}{:<12}".format("S.N", "ABONE NO", "ABONE ADI", "FAT.TUTARI", "ÖDE.TUTAR"))

        # for döngüsü ile her bir PDF dosyasını okuyup gerekli olan bilgileri çekiyoruz.
        for file in natsort.os_sorted(files):
            birincisayfa = []
            # pdfplumber modülü ile dosyaları açıyoruz
            with fitz.open(file) as pdf:
                sayfa_1 = pdf[0]
                text = sayfa_1.get_text()
                birincisayfa.append(text.replace('\n', ' ').upper())
            #print(birincisayfa)

            sira_no = sira_no + 1

            # Sayfaları Geziyoruz
            for sayfa in birincisayfa:
                # Cami/Abone adını tespit etmek için kriter belirliyoruz.
                abone_eslesen = re.search("(MÜŞTERİ ADI MÜŞTERİ ADI|ÖDEN\D+TUT\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)|MÜŞTERİ ADI)\s(\D+)", sayfa)
                abone_adres = ''.join(abone_eslesen.group(5)).strip().split(",")[0].split("MÜŞTERİ")[0]

                #aboneadres_eslesen = re.search("(MÜŞTERİ ADRESİ MÜŞTERİ ADRESİ|MÜŞTERİ ADRESİ|TESIS ADRESI)\s(\D+)", sayfa)
                #abone_adres = ''.join(aboneadres_eslesen.group(2)).split("ADRESİ")[-1].strip().split("OKUMA")[0].strip()

                # Toplam harcanan KWH
                kwh_eslesen = re.search("(K1\D+|T.KET.M)\s(\d+.\d+)", sayfa)
                if kwh_eslesen is None:
                    kwh_toplam = float(0)
                else:
                    kwh_toplam = ''.join(kwh_eslesen.group(2)).replace('.', '').replace(',', '.').strip()

                # Fatura Tesisat/Tüketici Numarası
                tesisat_eslesen = re.search("TES[Iİ]SAT\D+(\d+)", sayfa)
                tesisat_no = ''.join(tesisat_eslesen.groups()).strip()

                # Müsteri NO
                musteri_eslesen = re.search("M..TER.\sNO.?(\d+)", sayfa)
                musteri_no = ' '.join(musteri_eslesen.groups())

                # Fatura Tesisat/Tüketici Numarası
                sozhesap_eslesen = re.search("S.Z\D+HESA[PB].\s?\D+(\d+)", sayfa)
                sozhesap_no = ' '.join(sozhesap_eslesen.groups()).strip()

                # Fatura No
                faturano_eslesen = re.search("FATURA\D+NO:?\s(\D+\S+)", sayfa)
                fatura_no = ''.join(faturano_eslesen.groups()).strip()

                # Fatura Grubu
                faturagrubu_eslesen = re.search("GRUBU\D+\s(\D+)\s.G", sayfa)
                fatura_grubu = ' '.join(faturagrubu_eslesen.groups()).strip()

                # Son ödeme Tarihi
                sonOdeme_eslesen = re.search("(SON\D+HI:|Z)\s(\d+.\d+.\d+)", sayfa)
                sonOdeme_tarihi = ''.join(sonOdeme_eslesen.group(2)).strip()

                # İlk Okuma Tarihi
                ilkOkuma_eslesen = re.search("(MA\sG\D+|.LK\sOKUMA\D+)(\w+.\w+.\w+)", sayfa)
                ilkOkuma_tarihi = ''.join(ilkOkuma_eslesen.group(2))

                # Son Okuma Tarihi
                sonOkuma_eslesen = re.search("(MA\sG\D+\S+\D+|SON\sOKUMA\D+)(\w+.\w+.\w+)", sayfa)
                sonOkuma_tarihi = ''.join(sonOkuma_eslesen.group(2))

                # Faturanın ait olduğu dönem
                faturaDonemi_ay = sonOkuma_tarihi.split(".")[1]
                faturaDonemi_yil = sonOkuma_tarihi.split(".")[-1]
                fatura_Donemi = faturaDonemi_ay + "/" + faturaDonemi_yil

                # Faturada bulunan damga versigi
                damgavergisi_eslesen = re.search("DAMGA\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)
                if damgavergisi_eslesen is None:
                    damga_vergisi = 0
                else:
                    damga_vergisi = round(float(damgavergisi_eslesen.group(1).strip()), 2)

                #Faturada bulunan kdv tutarı
                kdv_eslesen = re.search("(KDV\s?\S+\s?([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?))", sayfa)
                kdv = round(float(kdv_eslesen.groups(1)[0].split()[-1].strip().replace(".", "").replace(',', '.')), 2)

                # # Gecikme Bedeli varsa
                # gecikmebedeli_eslesen = re.search("GEC.KME\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)
                # if gecikmebedeli_eslesen is None:
                #   gecikmebedeli = float(0)
                # else:
                #   gecikmebedeli = float(gecikmebedeli_eslesen.group(1).strip().replace(".", "").replace(',', '.'))

                # # Kesme/Açma Bedeli varsa
                # kesacbedeli_eslesen = re.search("KESME\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)
                # if kesacbedeli_eslesen is None:
                #   kesacbedeli = float(0)
                # else:
                #   kesacbedeli = float(kesacbedeli_eslesen.group(1).strip().replace(".", "").replace(',', '.'))

                # # trt fonu
                # trtfonu_eslesen = re.search("TRT\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)\sTL", sayfa)
                # if trtfonu_eslesen is None:
                #   trtfonu = float(0)
                # else:
                #   trtfonu = float(trtfonu_eslesen.group(1).strip().replace(".", "").replace(',', '.'))

                # # trafo fonu
                # trafo_eslesen = re.search("Trafo\D+Bo\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)\sTL", sayfa)
                # if trafo_eslesen == None:
                #   trafo = float(0)
                # else:
                #   trafo = float(trafo_eslesen.group(1).strip().replace(".", "").replace(',', '.'))

                # # enerji fonu
                # enerjifonu_eslesen = re.search("En\D+Fo\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)\sTL", sayfa)
                # if enerjifonu_eslesen is None:
                #   enerjifonu = float(0)
                # else:
                #   enerjifonu = float(enerjifonu_eslesen.group(1).strip().replace(".", "").replace(',', '.'))


                # # Elektrik Tükeyim Vergisi
                # elektriktuketimvergisi_eslesen = re.search("Elekt\D+T\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)\sTL", sayfa)
                # if elektriktuketimvergisi_eslesen is None:
                #   elektriktuketimvergisi = float(0)
                # else:
                #   elektriktuketimvergisi = float(elektriktuketimvergisi_eslesen.group(1).strip().replace(".", "").replace(',', '.'))

                FaturaTutari_eslesen = re.search("CAR\D+GÜNL\D+.*KWH(.*)ÖD", sayfa)
                odenecek_tutar_eslesen = re.search("ÖDEN\D+TUT\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)
                fatura_tutari_eslesen = re.search("FATU\D+TUT\D+([0-9]\d{0,2}?(.?\d{1,3})?(.?\d{1,3})?)", sayfa)

                if FaturaTutari_eslesen is not None:
                    odenecek_tutar = float(''.join(FaturaTutari_eslesen.groups()).strip().split()[-2].replace(".", "").replace(',', '.'))
                elif odenecek_tutar_eslesen is not None:
                    odenecek_tutar = float(''.join(odenecek_tutar_eslesen.group(1)).strip().replace(".", "").replace(',', '.'))
                else:
                    odenecek_tutar = 0.0

                if FaturaTutari_eslesen is not None:
                    fatura_tutari = float(''.join(FaturaTutari_eslesen.groups()).strip().split()[-4].replace(".", "").replace(',', '.'))
                elif fatura_tutari_eslesen is not None:
                    fatura_tutari = float(''.join(fatura_tutari_eslesen.group(1)).strip().replace(".", "").replace(',', '.'))
                else:
                    fatura_tutari = 0.0

            # Yaptığımız işlemin uçbirimden çıktısını görmek için print ile ekrana yazdırıyoruz.
            print("{:<5}{:<18}{:<40}{:<12}{:<12}".format(sira_no, sozhesap_no, abone_adres, fatura_tutari, odenecek_tutar))

            # ornek_excel.xlsx dosyasının son satırını buluyoruz.
            son_satir = excel_sayfasi.max_row

            # ornek_excel.xlsx dosyasının sırasıyla değerlerimizi yazdırıyoruz.
            excel_sayfasi.cell(column=1, row=son_satir + 1).value = int(sira_no)
            excel_sayfasi.cell(column=2, row=son_satir + 1).value = int(musteri_no)
            excel_sayfasi.cell(column=3, row=son_satir + 1).value = int(sozhesap_no)
            excel_sayfasi.cell(column=4, row=son_satir + 1).value = int(tesisat_no)
            excel_sayfasi.cell(column=5, row=son_satir + 1).value = fatura_no
            excel_sayfasi.cell(column=6, row=son_satir + 1).value = abone_adres
            excel_sayfasi.cell(column=7, row=son_satir + 1).value = fatura_grubu
            excel_sayfasi.cell(column=8, row=son_satir + 1).value = fatura_Donemi
            excel_sayfasi.cell(column=9, row=son_satir + 1).value = ilkOkuma_tarihi
            excel_sayfasi.cell(column=10, row=son_satir + 1).value = sonOkuma_tarihi
            excel_sayfasi.cell(column=11, row=son_satir + 1).value = sonOdeme_tarihi
            excel_sayfasi.cell(column=12, row=son_satir + 1).value = kwh_toplam
            excel_sayfasi.cell(column=13, row=son_satir + 1).value = fatura_tutari
            excel_sayfasi.cell(column=14, row=son_satir + 1).value = damga_vergisi
            excel_sayfasi.cell(column=15, row=son_satir + 1).value = kdv
            #excel_sayfasi.cell(column=16, row=son_satir + 1).value = gecikmebedeli
            #excel_sayfasi.cell(column=17, row=son_satir + 1).value = kesacbedeli
            excel_sayfasi.cell(column=16, row=son_satir + 1).value = odenecek_tutar

            # ornek_excel.xlsx dosyasını pdf_fatura_excel_aktarma.xlsx olarak kaydediyoruz.
            # pdf_fatura_excel_aktarma.xlsx olarak yeni bir excel dosyası hazırlamış oluyoruz.
            excel_dosyasi.save('pdf_fatura_excel_aktarma_' + tarih + '.xlsx')
    except:
        print("Dosya Adı: ", file)
        print(traceback.format_exc())

if __name__ == "__main__":
    fatura_oku()
